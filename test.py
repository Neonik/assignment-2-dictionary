import subprocess

input_data = ["second key", "bad", "fourth key", "very long key" * 15, "short"]
output_data = ["second value", "", "fourth value",  "", "value"]
error_message = "There is no such key"
print()

for i in range(len(input_data)):
    p = subprocess.Popen(["./code"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=input_data[i].encode())
    str_err = stderr.decode()
    str_out = stdout.decode()
    is_equlas = str_out == output_data[i];
    if is_equlas and ((output_data[i] != "" and str_err == "") or (output_data[i] == "" and str_err == error_message)):
        print(f"[{i+1}] -> OK")
    else:
        print(f"[{i+1}] -> FAIL")
print()
print("All tests are passed!")
print()
