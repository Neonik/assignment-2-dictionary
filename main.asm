%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define MAX_SIZE 256
%define OFFSET 8

section .rodata
    INCORRECT_INPUT_DATA: db "Incorrect input data", 0
    KEY_DOESNT_EXISTS: db "There is no such key", 0

section .bss
    input: resb MAX_SIZE

section .text

global _start
_start:
        mov rsi, MAX_SIZE
        mov rdi, input
        call read_stdin
        test rax, rax
        jz .incorrect_input

        mov rsi, first
        mov rdi, input
        call find_word
        test rax, rax
        jz .key_doesnt_exists

        add rax, OFFSET
        mov rbx, rax
        mov rdi, rax
        push rbx
        call string_length

        pop rbx
        add rax, rbx
        inc rax
        mov rdi, rax
        call print_string

        xor rdi, rdi
        jmp exit

    .incorrect_input:
        mov rdi, INCORRECT_INPUT_DATA
        jmp .print_err_and_exit

    .key_doesnt_exists:
        mov rdi, KEY_DOESNT_EXISTS

    .print_err_and_exit:
        call print_error
        mov rdi, 1
        jmp exit
