%include "colon.inc"

section .data

colon "fourth key", fourth_tag
db "fourth value", 0

colon "third key", third_tag
db "third value", 0

colon "short", short_tag
db "value", 0

colon "second key", second_tag
db "second value", 0

colon "first key", first_tag
db "first value", 0
