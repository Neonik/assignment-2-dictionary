%.o: %.asm
        nasm -felf64 -g $< -o $@

.PHONY: build
build: main.o lib.o dict.o
        ld -o code $^

.PHONY: test
test: build
        python3 test.py

.PHONY: clean
clean: 
        rm -rf code *.o

run: clean build test
